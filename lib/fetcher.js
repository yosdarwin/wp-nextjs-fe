const API_URL = "https://dstnct.masuk.id/graphql";

export async function fetcher(query, { variables } = {}) {
    const res = await fetch(API_URL, {
        method: "POST",
        headers: { "Content-Type": "application/json" },
        body: JSON.stringify({ query, variables }),
    });

    const json = await res.json();
    return json;
}
