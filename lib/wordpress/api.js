// Get 3 first post
export const ALL_POST = `
    query allKegiatanQuery {
        allKegiatan {
            nodes {
            id
            slug
            title
            excerpt {
                excerpt
            }
            }
        }
        }
        `;

// Get all post with slug
export const GET_ALL_KEGIATAN_WITH_SLUG = `
query AllKegiataSlug {
  allKegiatan(first: 10000) {
    nodes {
      slug
    }
  }
}
`;

// Get all post by slug
export const KEGIATAN_BY_SLUG = `
    query singleKegiatan($id: ID!, $idType: KegiatanIdType!) {
        kegiatan(id: $id, idType: $idType) {
            content
            date
            featuredImage {
                node {
                    sourceUrl
                }
            }
            title
            slug
        }
        }
`;
