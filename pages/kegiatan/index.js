import Link from "next/link";
import { useEffect, useState } from "react";
import { Posts } from "../../components/Posts";
import { fetcher } from "../../lib/fetcher";
import { ALL_POST } from "../../lib/wordpress/api";
import styles from "../../styles/Home.module.css";
import { POST_PER_PAGE } from "../../utils/constant";

export default function Home({ data }) {
    const [posts, setPosts] = useState([]);
    const [page, setPage] = useState(1);
    const [loading, setLoading] = useState(false);
    const [totalpages, setTotalpages] = useState(0);

    useEffect(() => {
        setLoading(true);
        function getAllPost() {
            setLoading(false);
            setPosts(data);
        }
        setTotalpages(Math.ceil(data.length / POST_PER_PAGE));

        getAllPost();
    }, []);

    const clickPaginate = (num) => {
        setPage(num);
    };

    return (
        <main className={styles.main}>
            <div className={styles.container}>
                <h1>All Kegiatan</h1>
                {loading ? (
                    <p>Loading....</p>
                ) : (
                    <Posts
                        clickPaginate={clickPaginate}
                        data={posts}
                        page={page}
                        totalpages={totalpages}
                    />
                )}
            </div>
        </main>
    );
}

export async function getStaticProps() {
    const res = await fetcher(ALL_POST);
    const data = res.data.allKegiatan.nodes;

    return {
        props: {
            data,
        },
        revalidate: 1,
    };
}
