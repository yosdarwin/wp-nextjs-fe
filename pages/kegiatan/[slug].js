import Image from "next/image";
import Link from "next/link";
import { useRouter } from "next/router";
import { fetcher } from "../../lib/fetcher";
import {
    KEGIATAN_BY_SLUG,
    GET_ALL_KEGIATAN_WITH_SLUG,
} from "../../lib/wordpress/api";
import styles from "../../styles/Home.module.css";
const singleKegiatan = ({ postData }) => {
    const itemKegiatan = postData.data.kegiatan;
    const Router = useRouter;

    return (
        <div>
            {Router.isFallback ? (
                <div>Loading...</div>
            ) : (
                <div className={styles.container}>
                    <h1>{itemKegiatan.title}</h1>
                    <div>
                        <Link href={"/kegiatan"}>
                            <a>Home</a>
                        </Link>
                    </div>
                    {itemKegiatan.featuredImage !== null && (
                        <img
                            src={itemKegiatan.featuredImage.node.sourceUrl}
                            alt="text"
                        />
                    )}

                    <article
                        dangerouslySetInnerHTML={{
                            __html: itemKegiatan.content,
                        }}
                    />
                </div>
            )}
        </div>
    );
};

export default singleKegiatan;

export async function getStaticPaths() {
    const res = await fetcher(GET_ALL_KEGIATAN_WITH_SLUG);
    const allPaths = await res.data.allKegiatan.nodes;

    return {
        paths: allPaths.map((path) => {
            return {
                params: {
                    slug: path.slug,
                },
            };
        }),
        fallback: false,
    };
}

export async function getStaticProps({ params }) {
    const variables = {
        id: params.slug,
        idType: "SLUG",
    };

    const data = await fetcher(KEGIATAN_BY_SLUG, { variables });
    return {
        props: {
            postData: data,
        },
    };
}
