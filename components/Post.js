import Link from "next/link";
export default function Post({ item }) {
    return (
        <div key={item.slug} className="card">
            <Link href={`/kegiatan/${item.slug}`}>
                <a>
                    <h2>{item.title}</h2>
                    <article>{item.excerpt.excerpt}</article>
                </a>
            </Link>
        </div>
    );
}
