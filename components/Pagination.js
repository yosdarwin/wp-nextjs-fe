const Pagination = ({ totalpages, clickPaginate, page }) => {
    const pages = [...Array(totalpages).keys()].map((num) => num + 1);
    return (
        <div>
            <button onClick={() => clickPaginate(1)}>First</button>
            {page > 1 && (
                <button onClick={() => clickPaginate(page - 1)}>Prev</button>
            )}
            {pages.map((num, index) => (
                <button key={index} onClick={() => clickPaginate(num)}>
                    {num}
                </button>
            ))}
            {page < totalpages && (
                <button onClick={() => clickPaginate(page + 1)}>next</button>
            )}
            <button onClick={() => clickPaginate(totalpages)}>last</button>
        </div>
    );
};

export default Pagination;
