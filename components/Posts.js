import { POST_PER_PAGE } from "../utils/constant";
import Post from "../components/Post";
import Pagination from "./Pagination";

export const Posts = ({ data, page, totalpages, clickPaginate }) => {
    const startPost = (page - 1) * POST_PER_PAGE;
    const selectedPost = data.slice(startPost, startPost + POST_PER_PAGE);
    return (
        <>
            <h3>Page {page}</h3>
            <div className="grid">
                {selectedPost.map((item) => {
                    return <Post key={item.slug} item={item} />;
                })}
            </div>
            <Pagination
                totalpages={totalpages}
                clickPaginate={clickPaginate}
                page={page}
            />
        </>
    );
};
